package delahayeyourself.hellocounter.utils;

/**
 *
 * @author sam
 */
public class Counter {
    private int counter;
    
    public Counter(){
        this.counter = 0;
    }
    
    public void increment(){
        this.counter += 1;
    }
    
    public void decrement(){
        this.counter -= 1;
    }
    
    public void raz(){
        this.counter = 0;
    }

    public int getCounter() {
        return counter;
    }
    
    
    
}
